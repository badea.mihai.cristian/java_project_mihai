package pachet_exemplu;

// Afiseaza daca prima litera e consoana sau vocala

import java.util.Scanner;

public class Exercitiu2 {
    public static void main(String[] args) {
        String a;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Introduceti litera dumneavoastra: ");
        a = scanner.nextLine();
        char b = a.charAt(0);
        if (b == 'a' || b == 'e' || b == 'i' || b == 'o' || b == 'u') {
            System.out.println("Litera este o vocala");
        } else {
            System.out.println("Litera e o consoana");
        }
    }
}
