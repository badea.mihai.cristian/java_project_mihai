package pachet_exemplu;

import java.util.Scanner;

public class Exercitiu {

    //Scrieti un array de int-uri in care se adauga o valoare in plus unui array.
    //Se citesc de la tastatura array-ul initial, valoarea care se adauga si pozitia sa in array.
    public static void main(String[] args) {
        Scanner citire = new Scanner(System.in);
        int[] array = new int[5];
        citireArrayInt(array);
        int[] array2 = adaugareVal(array);
        afisareInt(array2);
    }

    public static int[] citireArrayInt(int[] sir) {
        Scanner citire = new Scanner(System.in);
        for (int i = 0; i < sir.length; i++) {
            System.out.print("Nr." + (i + 1) + "= ");
            sir[i] = citire.nextInt();
        }
        return sir;
    }

    public static int[] adaugareVal(int[] sir) {
        Scanner citire = new Scanner(System.in);
        int index, valoare;
        System.out.print("Valoarea de introdus: ");
        valoare = citire.nextInt();
        System.out.print("Pozitia unde trebuie introdusa valoarea: ");
        index = citire.nextInt();
        int[] sirRetur = new int[6];
        for (int i = 0; i < index; i++) {
            sirRetur[i] = sir[i];
        }
        sirRetur[index - 1] = valoare;
        for (int i = sirRetur.length - 1; i >= index; i--) {
            sirRetur[i] = sir[i - 1];
        }
        return sirRetur;
    }

    public static void afisareInt(int[] sir) {
        System.out.print("Array-ul dumneavoastra este: ");
        for (int i = 0; i < sir.length; i++) {
            System.out.print(sir[i] + " ");
        }
    }
}


