package pachet_exemplu;

import java.util.Scanner;

public class Exercitiu3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Introduceti valoarea in cm: ");
        int x = scanner.nextInt();
        double z = modUnMas(x);
        System.out.println("Valoarea in inch este: " + z);
    }

    public static double modUnMas(int x) {
        double y = x / 2.54;
        return y;
    }
}
